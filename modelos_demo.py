# encoding:utf-8
#!/usr/bin/env python

import datetime
from google.appengine.ext import ndb


class RegistroContacto(ndb.Model):
    nombre = ndb.StringProperty()
    email = ndb.StringProperty()
    comentario = ndb.StringProperty()
    fecha_registro = ndb.DateTimeProperty(auto_now_add=True)

    def listar_registros(self):
        return RegistroContacto.query().fetch()


class LogTareas(ndb.Model):
    mensaje = ndb.StringProperty()
    fecha = ndb.DateTimeProperty(auto_now_add=True)


class UserModel(ndb.Model):
    nombre = ndb.StringProperty()
    email = ndb.StringProperty()
    picture = ndb.StringProperty()
    id_usuario = ndb.StringProperty()
    sexo = ndb.StringProperty(default='Nada')
    token = ndb.StringProperty()
    fecha_ingreso = ndb.DateTimeProperty(auto_now_add=True)
    fecha_login = ndb.DateTimeProperty(auto_now=True)

    def buscar_usuario(self, email):
        return UserModel.query(UserModel.email == email).get()

    def actualizar_fecha_login(self, usuario):
        usuario.fecha_login = datetime.datetime.now()
        return usuario.put()


class DailyTasksModel(ndb.Model):
	pass