# encoding:utf-8
#!/usr/bin/env python

import webapp2
import jinja2
import os
import httplib2
import json
import urllib
import urllib2

from google.appengine.api import taskqueue
from google.appengine.api import memcache
from google.appengine.api import mail
from apiclient.discovery import build
from oauth2client import client
from oauth2client.appengine import StorageByKeyName
from oauth2client.appengine import CredentialsModel

from modelos_demo import RegistroContacto
from modelos_demo import LogTareas
from modelos_demo import UserModel
import oauthconf


JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__) + '/templates'),
    autoescape=True,
    extensions=['jinja2.ext.autoescape']
)


class ComentariosHandler(webapp2.RequestHandler):

    @oauthconf.decorator.oauth_aware
    def get(self):

        if not oauthconf.decorator.has_credentials():
            variables = {
                'url': oauthconf.decorator.authorize_url(),
                'has_credentials': oauthconf.decorator.has_credentials(),
            }
            template = JINJA_ENVIRONMENT.get_template('login.html')
            self.response.write(template.render(variables))
        else:
            http = oauthconf.decorator.http()
            data = oauthconf.user_info_service.userinfo().get().execute(
                http=http)

            usuario = UserModel()
            usuario_iniciado = usuario.buscar_usuario(data['email'])

            if usuario_iniciado == None:
                usuario.nombre = data['name']
                usuario.email = data['email']
                usuario.picture = data['picture']
                usuario.sexo = data['gender']
                usuario.id_usuario = data['id']
                usuario.token = oauthconf.decorator.credentials.refresh_token
                usuario.put()

                storage = StorageByKeyName(CredentialsModel, data['id'], 'credentials')
                credentials = oauthconf.decorator.credentials
                storage.locked_put(credentials)
            else:
                usuario_iniciado.nombre = data['name']
                usuario_iniciado.picture = data['picture']
                usuario_iniciado.sexo = data['gender']
                usuario.id_usuario = data['id']
                usuario_iniciado.token = oauthconf.decorator.credentials.refresh_token
                usuario.actualizar_fecha_login(usuario_iniciado)
                usuario_iniciado.put()

            template = JINJA_ENVIRONMENT.get_template('index.html')
            self.response.write(template.render())

    def post(self):

        nombre = self.request.get('nombre')
        email = self.request.get('email')
        comentario = self.request.get('comentario')

        params = {
            'nombre': nombre.lower(),
            'email': email.lower(),
            'comentario': comentario.lower(),
        }

        q = taskqueue.Queue('registros')
        t = taskqueue.Task(url='/crearregistro', params=params)
        q.add(t)

        self.redirect('/')


class RegistrosHandler(webapp2.RequestHandler):

    def post(self):

        nombre = self.request.get('nombre')
        email = self.request.get('email')
        comentario = self.request.get('comentario')

        registro = RegistroContacto()

        registro.nombre = nombre
        registro.email = email
        registro.comentario = comentario

        registro.put()


class ListadoComentariosHandler(webapp2.RequestHandler):

    def get(self):

        registro = RegistroContacto()
        registros = registro.listar_registros()
        template = JINJA_ENVIRONMENT.get_template('listado_registros.html')
        context = {
            'registros': registros,
        }
        self.response.write(template.render(context))


class CrearColasHandler(webapp2.RequestHandler):

    def get(self):

        q = taskqueue.Queue('signatures')
        q.purge()
        q.add(
            taskqueue.Task(name="primer_task", payload="test", method='PULL'))
        self.response.write("asd")


class EjecutarColasHandler(webapp2.RequestHandler):

    def get(self):

        log = LogTareas()
        log.mensaje = self.request.get('payload')
        log.put()


class EnviarEmailHandler(webapp2.RequestHandler):

    def get(self):
        pass
        # mail.send_mail(
        #     sender="christiandiegor@gmail.com",
        #     to="crojas@azurian.com",
        #     subject="Probando email GAE",
        #     body="Hola esto es una prueba " +
        #     "de correo en app engine " +
        #     "saludos!")


class CronOauthHandler(webapp2.RequestHandler):

    def get(self):
        pass


class UsersHandler(webapp2.RequestHandler):

    @oauthconf.decorator.oauth_required
    def get(self):

        usuario = UserModel()
        yo = usuario.buscar_usuario('crojas@azurian.com')

        credentials = StorageByKeyName(CredentialsModel, yo.id_usuario, 'credentials').get()
        http = httplib2.Http()
        http = credentials.authorize(http)

        userinfo = build('oauth2', 'v2', http=http)

        data = userinfo.userinfo().get().execute()
        
        self.response.write(data)


app = webapp2.WSGIApplication(
    [('/', ComentariosHandler),
     ('/registros', ListadoComentariosHandler),
     ('/crearregistro', RegistrosHandler),
     ('/colas', CrearColasHandler),
     ('/ejecutar', EjecutarColasHandler),
     ('/usuarios', UsersHandler),
     # mail
     ('/email', EnviarEmailHandler),
     # cron con oauth2
     ('/cronoauth', CronOauthHandler),
     (oauthconf.decorator.callback_path,
      oauthconf.decorator.callback_handler()),
     ], debug=True)
